import { Component, OnInit } from '@angular/core';
import { ContentManagerService } from './services/content-manager.service';
import { User } from './model/user';
import { Issue } from './model/issue';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	contentManagerService: ContentManagerService;
	issues:Issue[] = [];
	errorMessage: string = '';
	error: boolean = false;
	spinner: boolean = true;
	openIssues: number = 0;
	closedIssues: number = 0;

	constructor(c:ContentManagerService) {
		this.contentManagerService = c;
	}

	ngOnInit() {
		this.loadIssues();
	}

	loadIssues(){
		let response = this.contentManagerService.getIssues().subscribe( data => {
			this.onLoadIssuesSuccess(data);
		}, err => {
			this.error = true;
			this.spinner = false;
			this.errorMessage = 'There was a problem loading the app. Please refresh your browser.';
		});
		
	}

	onLoadIssuesSuccess(data){
		this.issues = data;
		this.spinner = false;
		for (let issue of this.issues) {
			if(issue.state === 'open'){
				this.openIssues++
			} 
			if (issue.state === 'closed') {
				this.closedIssues++
			}
		}
	}

	close(){
		this.error = false;
		console.log('error')
	}
}
