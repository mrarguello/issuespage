import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ContentManagerService } from './services/content-manager.service';


@NgModule({
  declarations: [
 	 AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
 	 ContentManagerService
  ],
  bootstrap: [
 	 AppComponent
  ]
})
export class AppModule { }
