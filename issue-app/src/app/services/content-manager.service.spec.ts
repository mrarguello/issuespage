import { TestBed, inject } from '@angular/core/testing';
import { Http, HttpModule } from '@angular/http';
import { ContentManagerService } from './content-manager.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Issue } from '../model/issue';

describe('ContentManagerService', () => {

    let service: ContentManagerService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ContentManagerService],
            imports:[HttpModule, HttpClientTestingModule]
        });
        service = TestBed.get(ContentManagerService);
        httpMock = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should get Issues from the API'),() => {
        const dummyIssues: Issue[] = [{
            labels  : [{
                color  : 'test',
                default  : true,
                id  : 1,
                name  : 'test',
                url  : 'test'
            }],
            asignee  : 'test',
            repository_url : 'test',
            state : 'test',
            title : 'test',
            updated_at : 'test',
            number : 1,
            comments : 'test',
            created : 'test',
            updated : 'test',
            url : 'test',
            comments_url : 'test',
            creator : {
                id: 1,
                html_url: 'test',
                login: 'test',
                site_admin: true,
                type:'test'
            }
        }];

        service.getIssues().subscribe(issues => {
            expect(issues.length).toBe(2);
            expect(issues).toEqual(dummyIssues);
        });

        const request = httpMock.expectOne('https://api.github.com/repos/angular/angular.js/issues');
        expect(request.request.method).toBe('GET');

        request.flush(dummyIssues);
    }
});




