import { Injectable} from '@angular/core';
import { HttpModule, Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { User } from '../model/user';
import { Issue } from '../model/issue';

@Injectable()

export class ContentManagerService {

	constructor(private http: Http) { 
	}

	getIssues(): Observable<Issue[]>{	
		return this.http.get('https://api.github.com/repos/angular/angular.js/issues')
		.map(res => {
			return res.json().map(item => {
				return new Issue(
					item.labels,
					item.asignee,
					item.repository_url,
					item.state,
					item.title,
					item.updated_at,
					item.number,
					item.comments,
					item.created_at,
					item.updated_at,
					item.url,
					item.comments_url,
					new User(
						item.user.id,
						item.user.html_url,
						item.user.login,
						item.user.site_admin,
						item.user.type,
						),
					);
			});
		});
		
	}
}
