export interface Label{
	color: string;
	default: boolean;
	id: number;
	name: string;
	url: string
}