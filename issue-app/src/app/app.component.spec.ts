import { TestBed, async,  } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ContentManagerService } from './services/content-manager.service';
import { Http, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
       HttpModule
      ],      
      declarations: [
        AppComponent
      ],
      providers:[
      ContentManagerService
      ]

    }).compileComponents();
  }));


  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Issues List');
  }));

  it('should call the service to retrieve issues', () => {
    
    const fixture = TestBed.createComponent(AppComponent);
    let component = fixture.componentInstance;
    let service = TestBed.get(ContentManagerService);

    spyOn(service, 'getIssues').and.returnValue(Observable.of({'testData': 'test'}));
    component.loadIssues();
    expect(service.getIssues).toHaveBeenCalled();

  });

  it('should send success responses to onLoadIssuesSuccess',() => {
    const fixture = TestBed.createComponent(AppComponent);
    let component = fixture.componentInstance;
    let service = TestBed.get(ContentManagerService);

    spyOn(component, 'onLoadIssuesSuccess');
    spyOn(service, 'getIssues').and.returnValue(Observable.of({'testData': 'test'}));
    component.loadIssues();
    expect(component.onLoadIssuesSuccess).toHaveBeenCalled();
  })

});
