export class User {
	constructor(
		public id: number,
		public html_url: string,
		public login: string,
		public site_admin: boolean,
		public type:string
		){
	}
}