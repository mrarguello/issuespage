import { Label } from '../interfaces/label';
import { User } from '../model/user';

export class Issue {
	constructor(
		public labels : Label[],
		public asignee : string,
		public repository_url: string,
		public state: string,
		public title: string,
		public updated_at: string,
		public number: number,
		public comments: string,
		public created: string,
		public updated: string,
		public url: string,
		public comments_url: string,
		public creator: User
		){
	}
}